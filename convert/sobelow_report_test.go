package convert

import (
	"reflect"
	"testing"
)

var sreport = SoBelowReport{
	Findings: SoBelowConfidenceGroups{
		MediumConfidence: []SoBelowVulnerability{
			{
				Type: "Traversal.SendDownload: Directory Traversal in `send_download`",
				File: "lib/app_web/controllers/root_controller.ex",
				Line: 42,
			},
		},
	},
	SoBelowVersion: "0.8.0",
	TotalFindings:  1,
}

func TestSoBelowReportVulnerabilityCompareKey(t *testing.T) {
	want := "lib/app_web/controllers/root_controller.ex:42:traversal_in_send_download"
	got := sreport.Findings.MediumConfidence[0].compareKey()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestSoBelowReportVulnerabilityTypeSlug(t *testing.T) {
	want := "traversal_in_send_download"
	got := sreport.Findings.MediumConfidence[0].typeSlug()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestSoBelowReportVulnerabilityMessageWhenSupportedType(t *testing.T) {
	want := "Directory Traversal in `send_download`"
	got := sreport.Findings.MediumConfidence[0].message()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
