package convert

import (
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

func TestConvert(t *testing.T) {
	in := `{
		"findings": {
			 "high_confidence": [
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 10,
						"type": "XSS.ContentType: XSS in ` + "`put_resp_content_type`" + `",
						"variable": "unsafe_param"
					},
					{
						"file": "config/prod.exs",
						"line": 0,
						"type": "Config.HTTPS: HTTPS Not Enabled"
					},
					{
						"file": "config/prod.secret.exs",
						"key": "secret_key_base",
						"line": 12,
						"type": "Config.Secrets: Hardcoded Secret"
					},
					{
						"file": "lib/sample_app_web/router.ex",
						"line": 4,
						"pipeline": "browser",
						"type": "Config.Headers: Missing Secure Browser Headers"
					},
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 16,
						"type": "SQL.Query: SQL injection",
						"variable": "sql"
					},
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 24,
						"type": "RCE.EEx: Code Execution in ` + "`EEx.eval_string`" + `",
						"variable": "unsafe_param"
					},
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 32,
						"type": "DOS.StringToAtom: Unsafe ` + "`String.to_atom`" + `",
						"variable": "unsafe_template"
					},
					{
						"file": "lib/sample_app_web/controllers/page_controller.ex",
						"line": 37,
						"type": "CI.OS: Command Injection in ` + "`:os.cmd`" + `",
						"variable": "unsafe_cmd"
					},
					{
						"file": "lib/sample_app_web/router.ex",
						"line": 4,
						"pipeline": "browser",
						"type": "Config.CSRF: Missing CSRF Protections"
					}
				],
				"low_confidence": [
				],
				"medium_confidence": [
				]
		},
		"sobelow_version": "0.8.0",
		"total_findings": 9
	}`

	scanner := report.Scanner{
		ID:   "sobelow",
		Name: "Sobelow",
	}

	r := strings.NewReader(in)
	want := &report.Report{
		Version: report.CurrentVersion(),
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    report.CategorySast,
				Scanner:     &scanner,
				Name:        "XSS in `put_resp_content_type`",
				Description: "Cross-Site Scripting: Cross-Site Scripting (XSS) vulnerabilities are a result of rendering untrusted input on a page without proper encoding. XSS may allow an attacker to perform actions on behalf of other users, steal session tokens, or access private data. Read more about XSS here: https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:10:xss_in_put_resp_content_type",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 10,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID xss_in_put_resp_content_type",
						Value: "xss_in_put_resp_content_type",
					},
				},
			},
			{
				Category:    report.CategorySast,
				Scanner:     &scanner,
				Name:        "HTTPS Not Enabled",
				Description: "HTTPS: Without HTTPS, attackers in a priveleged network position can intercept and modify traffic. Sobelow detects missing HTTPS by checking the prod configuration.",
				CompareKey:  "config/prod.exs:0:config_https_not_enabled",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:      "config/prod.exs",
					LineStart: 0,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID config_https_not_enabled",
						Value: "config_https_not_enabled",
					},
				},
			},
			{
				Category:    report.CategorySast,
				Scanner:     &scanner,
				Name:        "Missing Secure Browser Headers",
				Description: "Missing Secure HTTP Headers: By default, Phoenix HTTP responses contain a number of secure HTTP headers that attempt to mitigate XSS, click-jacking, and content-sniffing attacks. Missing Secure HTTP Headers is flagged by `sobelow` when a pipeline accepts \"html\" requests, but does not implement the `:put_secure_browser_headers` plug.",
				CompareKey:  "lib/sample_app_web/router.ex:4:config_missing_secure_browser_headers",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:      "lib/sample_app_web/router.ex",
					LineStart: 4,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID config_missing_secure_browser_headers",
						Value: "config_missing_secure_browser_headers",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "SQL injection",
				Description: "SQL Injection: SQL injection occurs when untrusted input is interpolated directly into a SQL query. In a typical Phoenix application, this would mean using the `Ecto.Adapters.SQL.query` method and not using the parameterization feature. Read more about SQL injection here: https://www.owasp.org/index.php/SQL_Injection",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:16:sql_injection",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelHigh,
				Scanner:     &scanner,
				Location: report.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 16,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID sql_injection",
						Value: "sql_injection",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "Code Execution in `EEx.eval_string`",
				Description: "Insecure EEx evaluation: If user input is passed to EEx eval functions, it may result in arbitrary code execution. The root cause of these issues is often directory traversal.",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:24:rce_in_eex_eval_string",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelHigh,
				Scanner:     &scanner,
				Location: report.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 24,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID rce_in_eex_eval_string",
						Value: "rce_in_eex_eval_string",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "Unsafe `String.to_atom`",
				Description: "Denial of Service via `String.to_atom`: In Elixir, atoms are not garbage collected. As such, if user input is passed to the `String.to_atom` function, it may result in memory exhaustion. Prefer the `String.to_existing_atom` function for untrusted user input.",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:32:dos_unsafe_string_to_atom",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelHigh,
				Scanner:     &scanner,
				Location: report.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 32,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID dos_unsafe_string_to_atom",
						Value: "dos_unsafe_string_to_atom",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "Command Injection in `:os.cmd`",
				Description: "Command Injection: Command Injection vulnerabilities are a result of passing untrusted input to an operating system shell, and may result in complete system compromise. Read more about Command Injection here: https://www.owasp.org/index.php/Command_Injection",
				CompareKey:  "lib/sample_app_web/controllers/page_controller.ex:37:ci_in_os_cmd",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelHigh,
				Scanner:     &scanner,
				Location: report.Location{
					File:      "lib/sample_app_web/controllers/page_controller.ex",
					LineStart: 37,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID ci_in_os_cmd",
						Value: "ci_in_os_cmd",
					},
				},
			},
			{
				Category:    "sast",
				Name:        "Missing CSRF Protections",
				Description: "Cross-Site Request Forgery: In a Cross-Site Request Forgery (CSRF) attack, an untrusted application can cause a user's browser to submit requests or perform actions on the user's behalf. Read more about CSRF here: https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF). Cross-Site Request Forgery is flagged by `sobelow` when a pipeline fetches a session, but does not implement the `:protect_from_forgery` plug.",
				CompareKey:  "lib/sample_app_web/router.ex:4:config_missing_csrf_protections",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelHigh,
				Scanner:     &scanner,
				Location: report.Location{
					File:      "lib/sample_app_web/router.ex",
					LineStart: 4,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "sobelow_rule_id",
						Name:  "Sobelow Rule ID config_missing_csrf_protections",
						Value: "config_missing_csrf_protections",
					},
				},
			},
		},
		Analyzer: "sobelow",
		Config:   ruleset.Config{Path: ruleset.PathSAST},
	}

	got, err := Convert(r, "", nil)
	if err != nil {
		t.Fatal(err)
	}

	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Wrong result. (-want +got):\n%s", diff)
	}
}
