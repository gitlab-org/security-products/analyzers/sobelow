package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/sobelow/v5/metadata"
)

const (
	ignoredModules = "Vuln" // disable dependency_scanning module
	pathOutput     = "sobelow.json"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func loadRulesetConfig(projectPath string) (*ruleset.Config, error) {
	// Load custom config if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	return ruleset.Load(rulesetPath, metadata.AnalyzerID, log.StandardLogger())
}

func analyze(_ *cli.Context, repositoryPath string, _ *ruleset.Config) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = repositoryPath
		cmd.Env = os.Environ()
		return cmd
	}

	args := []string{
		"sobelow",
		"--strict",  // cause sobelow to exit if it encounters a bad ast
		"--private", // disable outbound version check
		"--format", "json",
		"--ignore", ignoredModules,
		"--out", pathOutput, // set output file
	}

	cmd := setupCmd(exec.Command("mix", args...))
	log.Debug(cmd.String())

	// all output from the scanner command is logged using the Writer() default `info` level
	streamingInfoLogger := log.StandardLogger().Writer()
	defer streamingInfoLogger.Close()
	// Use the streamingInfoLogger to immediately log each line that sobelow outputs as it executes
	cmd.Stderr = streamingInfoLogger
	cmd.Stdout = streamingInfoLogger

	if err := cmd.Run(); err != nil {
		log.Errorf("command exec failure \n command: '%s'\n error: %v", cmd.String(), err)
		return nil, err
	}

	return os.Open(filepath.Clean(filepath.Join(repositoryPath, pathOutput)))
}
